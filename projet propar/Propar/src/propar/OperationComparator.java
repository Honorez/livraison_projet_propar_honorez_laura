package propar;

import java.util.Comparator;


public class OperationComparator implements Comparator<Operation> {
 
	/**
	 * Compare les noms des employ�s pour le tri
	 */
	public int compare(Operation o1, Operation o2) {
		return o1.getNomEmploye().compareTo(o2.getNomEmploye());
	}
 
}