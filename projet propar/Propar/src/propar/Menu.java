package propar;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import mesExceptions.CaracteresInvalidesException;
import mesExceptions.SaisiVideException;

public class Menu {
	public static List<Employe> employes = new ArrayList<Employe>();
	static String utilisateur = "";
	static String utilisateurNom = "";
	static String utilisateurPrenom = "";
	static String utilisateurLogin = "";
	static List<Operation> operations;

	/**
	 *  V�rification pour la connexion
	 * @param login
	 * @param mdp
	 */
	public static void connexion(String login, String mdp) {
		Scanner lire = new Scanner(System.in);
		boolean etatconnexion = false;
		int i = 0;
		while (etatconnexion == false) {
			for (Employe element : employes) { // Exception si rien est trouv�
				if ((login.equals(element.getLogin())) && (mdp.equals(element.getMdp()))) {
					element.setNom(element.getNom());
					element.setPrenom(element.getPrenom());
					utilisateur = element.getGrade();
					utilisateurNom = element.getNom();
					utilisateurPrenom = element.getPrenom();
					utilisateurLogin = element.getLogin();
					etatconnexion = true;
					System.out.println("Bienvenue " + element.getPrenom() + " " + element.getNom());
					break;
				}
			}
			i++;
			if (etatconnexion == true) {
				break;
			}
			if (i >= employes.size()) {
				System.out.println("Login ou Mot de passe incorrect, veuillez r�essayer");
				System.out.println("Login :");
				login = lire.nextLine();
				System.out.println("Mot de passe :");
				mdp = lire.nextLine();
				i = 0;
			}
		}
	}
	
	/**
	 * Menu principal
	 */

	public static void menu1() {
		Scanner lire = new Scanner(System.in);
		String sousmenu = "";
		boolean arret = false;
		while (!arret) {
			System.out.println(" ");
			System.out.println("----------------Propar----------------");
			System.out.println("A - Connexion ");
			System.out.println("B - Lister toutes les op�rations en cours ");
			System.out.println("C - Lister toutes les op�rations termin�es");
			System.out.println("D - Quitter");
			sousmenu = lire.nextLine();
			switch (sousmenu) {
			case "A": {
				System.out.println("---------------Connexion----------------------");
				Menu menu = new Menu();
				String login = "";
				String mdp = "";
				Scanner lire2 = new Scanner(System.in);
				System.out.println("Login :");
				login = lire2.nextLine();
				System.out.println("Mot de passe :");
				mdp = lire2.nextLine();
				Menu.connexion(login, mdp);
				Menu.menu2();
			}
				break;

			case "B": {
				System.out.println("-------Lister toutes les op�rations en cours-----");
				Employe.listerOperation();
				break;
			}

			case "C": {
				System.out.println("-----Lister toutes les op�rations termin�es------");
				Employe.listerOperationTermine();
			}
				break;

			case "D":
				System.out.println("Fin du programme");
				arret = true;
				break;
			default:
				System.out.println("Saisie invalide, veuillez choisir parmi les lettres propos�es");
				break;
			}

		}
	}
/**
 * Menu apr�s connexion
 */
	public static void menu2() {
		Scanner lire = new Scanner(System.in);
		String sousmenu = "";
		boolean arret = false;
		while (!arret) {
			System.out.println(" ");
			System.out.println("----------------Propar----------------");
			System.out.println(" ");
			System.out.println("A - Ajouter une op�ration ");
			System.out.println("B - Terminer une op�ration ");
			System.out.println("C - Lister ses op�rations");
			System.out.println("D - Cr�er un employ�");
			System.out.println("E - Voir les recettes");
			System.out.println("F - Deconnexion");
			sousmenu = lire.nextLine();
			switch (sousmenu) {
			
			case "A": {
				System.out.println("---------------Ajouter une op�ration----------------------");
				try {
					Employe.ajouterOperation(employes, utilisateurLogin);
				} catch (SaisiVideException e) {
					e.printStackTrace();
				} catch (CaracteresInvalidesException e) {
					e.printStackTrace();
				}
				break;
			}

			case "B": {
				System.out.println("-------Terminer une op�ration-----");
				Employe.terminerOperation(employes, utilisateurLogin);
				break;
			}

			case "C": {
				System.out.println("-----Lister ses op�rations------");
				Employe.listerOperationPerso(utilisateurLogin);
				break;
			}


			case "D": {
				System.out.println("-----Cr�er un employ�------");
				if (utilisateur.equals("Expert")) {
					try {
						Expert.ajouterPersonnel(employes);
					} catch (SaisiVideException e) {
						e.printStackTrace();
					} catch (CaracteresInvalidesException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("Vous n'avez pas l'autorisation d'effectuer cette action.");
				}
				break;
			}

			case "E": {
				System.out.println("-----Voir les recettes------");
				if (utilisateur.equals("Expert")) {
					Operation.calculRecette(operations);
				} else {
					System.out.println("Vous n'avez pas l'autorisation d'effectuer cette action.");
				}
				break;
			}
				
				
				
			case "F":
				System.out.println("Deconnexion");
				arret = true;
				break;
			default:
				System.out.println("Saisie invalide, veuillez choisir parmi les lettres propos�es");
				break;
			}

		}
	}
}
